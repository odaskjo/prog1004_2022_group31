/** \brief
 *
 * \param
 * \param
 *
 *  @file       PROG1004_group31.cpp
 *  @author     Oda Skj�lberg, group31
 */


#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <math.h>
#include <iomanip>             //  setprecision
#include <cctype>              //  toupper
#include <cstdlib>             //  atoi, atof
#include <Windows.h>           //  atoi, atof
using namespace std;

// ENUMS
//.................................................
/**
 *  Type of match (what round in the tournament it is)
 */
enum MatchType {unDefined, qualifier, quarterFinal, semiFinal, finale};

/**
 *  Sets winner of a match, or draw..
 */
enum MatchOutcome {notSet, teamOne, teamTwo, draw};

// CLASSES
//.................................................
/**
 *  Player class, with name, number, goals scored,
 *  penalties and ban-status
 */
class Player {
private:
    string name;
    int number;
    int goalsScored;
    int penalties;                      // initialised to zero, total accumulated
    bool suspended;                     // false by default, true if one red card or three yellow
    int yellowCards;
    int redCard;
public:
    Player();
    Player(int pNr);                    // overloaded constructor, reads name
    Player(ifstream & in);              // file reading constructor, reads variables from file
 //   ~Player();
    void readPlayer(int nr);            // sets name and number from user input -> doesn't work
    void readPlayer(string n, int nr);  // overloaded, sets name and number from arguments -> doesn't work
    void setPlayerNr(int nr);           // sets number from arguments
    void printPlayer();                 // prints player variables to screen, name and number to start with
    void printPlayerGoals();            // prints player goals
    void printPlayerPenalties();        // prints player penalties
    void addGoals(int g);               // adds goals scored
    void addYellow();                   // adds yellow card, increases penalties counter, if yellowcards >= 3, isSuspended = true
    void addRed();                      // adds red card, increases penalties counter, sets suspended = true
    int returnPlayerNr();               // returns player number
    int retGoals();                     // returns player goals
    string returnPlayerName();          // returns player number
    bool isSuspended();                 // whether or not player is suspended from further playing in the current match
    bool hasPenalties();                // returns true if the player has received penalties
    void toFile(ofstream & out) const;        // writes player info to file
};
//.................................................
/**
 *  Team class, with a list of players, a trainer,
 *  location, and goals scored
 */
class Team {
private:
    vector <Player*> vPlayers;      // vector with all the players in the specific team
    string coach;                   // name of the team coach
    string city;                    // city of origin
    string name;                    // team location/city/name
    int goalsScored;                // goals scored so far in the tournament, for stats
    int minusGoals;                 // goals scored against the team so far in the tournament
    int deltaGoals;                 // set from match-function, goals of this team minus opposing team's goals
    int teamNr;                     // number of the team <-- might not need this
    int numPlayers;                 // total number of players in the team, needed for function iteration
    int wins, losses, draws;        // for stats
    int totalPoints;                // 2 pts for each win, 1 for draw, 0 for loss
    int matchesPlayed;              // updates after every match, for stats
    int penalties;                  // total penalties, all players in the team
public:
    Team();                         // empty constructor, to be declared below
    Team(int tNr);                  // overloaded constructor with parameter
    Team(ifstream & in);            // constructor, reads team from file
    ~Team();
    void registerTeam();            // user inputs team variables and adds players to vPlayers
    void registerTeam(int tNr);     // user inputs team variables and adds players to vPlayers
    void printTeam();               // prints info to screen
    void printTeamGoalsPlayers();   // prints team info, plus goals scored by players
    void printTeamGoalsMain();      // prints team name, plus goals, deltagoals, and minusgoals
    void printTeamResults();        // prints team results: totalpoints, wins, losses, draws
    void printMainInfo();           // prints main info to screen (name, city, numPlayers)
    void printTeamNr();             // prints team nr only
    void addWLD(int w, int l, int d); // increments the wins, losses, draws values respectively
    void addGoals(int gScored);     // adds goals
    void addMinusGoals(int mG);     // adds minusgoals, called after goals have been added to opposing team in a match
    void addDeltaGoals();           // calculates deltaGoals, from match function
    void viewStats();               // returns wins, losses, draws, number of penalties
    void addMatchPoints(int matchPts);     // adds points to total points after each match, based on wins, not goals
    void addPenalties();            // adds penalties to specified players
    void viewPenalties();           // prints penalties of the players that have them
    string returnNameOnly();        // returns team name as string value
    int returnTeamNr();             // returns team nr as int value
    int getTotalPoints();           // returns totalPoints, i.e. total accumulated match scores
    int returnGoalsScored();        // returns goalsScored, i.e. total accumulated goals scored by team players
    int returnDeltaGoals();         // returns deltaGoals,
    bool hasPenalties();            // returns true if the teams has any penalties registered
    Player* returnPlayer(int nr);   // returns a pointer to a player on the team with number = nr
    vector <Player*> retPlayers();  // returns the entire player vector of the team
    void toFile(ofstream & out) const;    // writes team info to file
    friend void addTeam();



};
//.................................................
/**
 *  Match class, with two opposing teams, win&loss,
 *  score (added after completing a match), tournament round
 */
class Match {
private:
    vector <Team*> vSetup;      // the two teams in the match
    int goals1, goals2;         // goals scored by team 1 and 2
    int matchNr;
    bool completed;             // set to true after the match is completed
    bool created;               // set to true after match creation
    //..........................// move these to protected if you're gonna use the virtuals instead:
    MatchType type;             // based on tournament round
    MatchOutcome outcome;       // sets winner of the match or draw
protected:

public:
    Match();
    Match(int n);
    Match(ifstream & in);
    ~Match();
    void enterTeams();          // allows user to add TWO teams to vSetup
    void enterTeams(Team* t1,Team* t2);          // enters two teams in vSetup
    void enterTeam1(Team* t1);
    void enterTeam2(Team* t2);
    void setAsQuali();
    void setAsQuarter();
    void setAsSemi();
    void setAsFin();
    void viewTeams();           // views the two teams registered in the vector vTeams
    void addMatchStats();       // updates goals scored
    void registerMatchOutcome();// asks user if all goals have been input, then decides on outcome
    void printMainInfo();       // prints team names and numbers to screen
    void setResult();           // sets enum value, sets completed=true, and calls Team::addMatchPoints(..)
    void viewMatchResult();     // prints who won to screen based on matches in a round
    Team* compareGoals();       // returns the team with the highest score, in case of a draw
    bool isCreated();           // returns true if the match is created with two teams in vSetup
    bool isComplete();          // returns true if the match is completed and has an outcome (team1, team2 or draw)
    int returnMatchNr();        // returns the variable matchNr
    Team* returnWinner();       // Returns pointer to winning team, or nullptr
    Team* returnLoser();        // Returns pointer to loser team, or nullptr
    void toFile(ofstream & out) const;

};

//.................................................

//.................................................
// GLOBAL FUNCTIONS
//.................................................
void displayMenu();
void teamMenu();
void matchMenu();
void goalsMenu();
void resultsMenu();
void statsMenu();
void editTournament(); // possibly redundant?
void addTeam();
void viewTeam();
void viewAllTeams();
void viewAllTeamGoals();
void addPenalty();
void finalize();
void viewResults();
void viewMatchResults();
void viewPlayerRankings();
void viewTeamRankings();
void addGoalsScored();
void createMatchSetup();
void viewInitMatchSetup();
void viewMatchSetup();
void increaseRoundParam(vector <Match*> m);
void team();
void matches();
void goals();
void stats();
void results();
char readChar(const char* t);
int readInt(const char* t, const int min, const int max);
Team* findTeam(const string n);     // returns team from gTeams based on name (if found)
Team* findTeam(const int nr);       // returns team from gTeams based on number (if found)
void readTeamsFromFile();           // reads teams from file
void readMatchesFromFile();         // reads matches from file
void writeTeamsToFile();            // writes to file
void writeMatchesToFile();          // writes to file

//.................................................
// GLOBAL VARIABLES
//.................................................
vector <Team*> gTeams;                  ///< all teams in the tournament, with vector of players
vector <Match*> gMatches;               ///< all matches played, each with identifier (quali, quarterF, semiF, f)
Team* gWinner;
Match* gFinale;                         // only one, roundParam=4
vector <Match*> gSemifinals;            // semi finalists, roundParam=3
vector <Match*> gQuarters;              // quarter finalists, roundParam=2
vector <Match*> gQualifiers;            // qualifying matches, roundParam=1
vector <Team*> qualiWinners;
vector <Team*> quarterWinners;
vector <Team*> semiWinners;
int roundParam = 1;                     // used in switch, displays current results, updated after each round increment
const int  MAX_CHAR = 200;              ///< used in the readChar-function, limits input buffer
int numTeams = 0;
int roundCreated = 0;                   // used in switch, displays match setup before it's completed
const int MAX_TEAMS = 16;
HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);
void fontsize(int,int);
bool anyPenalties();                    // true if any teams have got any penalties yet



//.................................................
/**
 *  Main program
 *
 */
int main(){
    system("COLOR 09");
    fontsize(30, 30); // works, but might as well change that in the window
    char command;
    char answer1, answer2;
    answer1 = readChar("\n\tRead existing teams and matches from file? (Y/N)");

    if(answer1=='Y'){
        readTeamsFromFile();
        readMatchesFromFile();
    }

    displayMenu();
    command = readChar("Please enter command");

    while(command != 'Q'){
        switch(command){
            case 'T':   team();         break;
            case 'M':   matches();      break;
            case 'G':   goals();        break;
            case 'R':   results();      break;
            case 'S':   stats();        break;
            default:  cout << "\tInvalid command\n" << endl; /* displayMenu();*/  break;
        }
        displayMenu();
        command = readChar("Please enter command");
    }

    answer2 = readChar("\n\tSave team and match info to file? (Y/N)");

    if(answer2=='Y'){
        writeTeamsToFile();
        writeMatchesToFile();
    }

}

//-------------------------------------------------
//          CLASS-FUNCTION DEFINITIONS
//-------------------------------------------------
// Player()
//.................................................
/**
 *  Player constructor, initializes and sets all variables to zero
 */
Player::Player(){
    number = goalsScored = yellowCards = redCard = penalties = 0;
    name = nullptr;
    suspended = false;
}
/**
 *  Overloaded player constructor, initializes and sets all variables to zero
 */
Player::Player(int pNr){
    goalsScored = yellowCards = redCard = penalties = 0;
    suspended = false;

    number = pNr;
    cout << "\tName of player nr. " << number << ": "; getline(cin,name);
}

/**
 *  Function that reads player variables from file
 *
 *  @param  in - file that is read from
 */
Player::Player(ifstream & in){
    in >> number; in.ignore();
    getline(in,name);
    in >> penalties >> yellowCards >> redCard; in.ignore();
    in >> goalsScored; in.ignore();
}

/**
 *  Function that reads new players from parameters
 */
void Player::readPlayer(string n, int nr){
    name = n;
    number = nr;
}

/**
 *  Function that sets player number from parameter
 *
 *  @param  nr  - player number
 */
void Player::setPlayerNr(int nr){
    number = nr;
}

/**
 *  Prints player to screen, prints name, number, coach
 */
void Player::printPlayer(){
    cout << setw(20) << name << setw(5) << number << endl;
}

/**
 *  Prints player to screen, prints name, number, coach
 */
void Player::printPlayerGoals(){
    cout << setw(20) << name << setw(5) << number << setw(10) << goalsScored << endl;
}

/**
 *  Prints player penalties to screen
 */
void Player::printPlayerPenalties(){
    cout << setw(20) << name <<  setw(7) << ", nr." << number << setw(20) << yellowCards << setw(20) << redCard << endl;
}

/**
 *  Function that lets adds goals scored by player to player
 *
 *  @param  g - number of goals to be added
 */
void Player::addGoals(int g){
    goalsScored += g;
}

/**
 *  Adds yellow cards to the player, if it reaches 3 yellow cards, it turns into a red one
 *  and the player is suspended. Then increments penalties counter
 */
void Player::addYellow(){
    yellowCards += 1;
    if(yellowCards>=3){
        redCard += 1;
        yellowCards -=1;
        suspended = true;
    }
    penalties += 1;
}

/**
 *  Adds red cards to the player, increments penalties counter, sets suspended to true
 */
void Player::addRed(){
    redCard += 1;
    suspended = true;
    penalties += 1;
}

/**
 *  Returns the int value of the number variable of the player
 *
 *  @return number - player number returned
 */
int Player::returnPlayerNr(){
    return number;
}

/**
 *  Returns the int value of the goals scored by the player
 *
 *  @return goalsScored - player goals scored
 */
int Player::retGoals(){
    return goalsScored;
}

/**
 *  Returns the string value of the name variable of the player
 *
 *  @return name - player name returned
 */
string Player::returnPlayerName(){
    return name;
}

/**
 *  Function that returns true if the player is suspended, false otherwise
 *
 *  @return true - if player is suspended, false otherwise
 */
bool Player::isSuspended(){
    if(suspended){
        return true;
    }
    else
        return false;
        // could just return the suspended boolean, as it is either true or false itself..
}

/**
 *  Function that returns true if the player has a penalty
 *
 *  @return true - if player is has received a penalty
 */
bool Player::hasPenalties(){
    if(penalties>0){
        return true;
    }
    else
        return false;
}

/**
 *  Writes player info to file
 *
 *  @param out - output file destination that is written to
 */
void Player::toFile(ofstream & out) const {
    out << number << '\n';
    out << name << '\n';
    out << penalties << ' ' << yellowCards << ' ' << redCard << '\n';
    out << goalsScored << '\n';
}



//-------------------------------------------------
// Team()
//.................................................
/**
 *  Team constructor, initializes and sets variables to zero
 */
Team::Team(){

    totalPoints = matchesPlayed = penalties = numPlayers = 0;
    deltaGoals = minusGoals = goalsScored = 0;
    name = coach = nullptr;
    vPlayers = {};
}

/**
 *  Overloaded team constructor, initializes variables
 */
Team::Team(int tNr){

    totalPoints = matchesPlayed = penalties = numPlayers = 0;
    teamNr = tNr;
    deltaGoals = minusGoals = goalsScored = 0;
    Player* player = nullptr;

    cout << "\tPlease enter name and city of the team: ";
    cout << "\n\tName: "; getline(cin,name);
    cout << "\tCity: "; getline(cin,city);
    cout << "\tEnter name of team coach: ";     getline(cin,coach);
    numPlayers = readInt("\tHow many players will be registered on the team?",1,30);

    for(int i = 1; i <= numPlayers; i++){
        player = new Player(i);      // Initialises a new player, with all values 0

        vPlayers.push_back(player);
    }
    numTeams+=1;
}

/**
 *  Team constructor, reads info from file
 *
 *  @param  in - file destination, the file that is read from
 */
Team::Team(ifstream & in){


    totalPoints = matchesPlayed = penalties = numPlayers = teamNr = 0;
    deltaGoals = minusGoals = goalsScored = 0;

    Player* player = nullptr;

    getline(in, name);
    getline(in, city);
    getline(in, coach);

    in >> teamNr >> totalPoints >> penalties; in.ignore();
    in >> goalsScored >> deltaGoals >> minusGoals; in.ignore();

    in >> numPlayers; in.ignore();

    for(int i = 1; i <= numPlayers; i++){
        player = new Player(in);      // reads player from file

        vPlayers.push_back(player);
    }
    // in.ignore();
}

/**
 *  Destructor, removing objects and pointers from vPlayers
 */
Team::~Team(){
    while (!vPlayers.empty()) {                     //  Still elements in vPlayers
        delete vPlayers.back();                     //  Deleting last element, removing last pointer reference
        vPlayers.pop_back();
    }
}

/**
 *  Function that adds new players to the team, also adds name, city and coach
 *
 *  @param  tNr - team number, placement+1 in the vector gTeams
 */
void Team::registerTeam(int tNr){
    teamNr = tNr;
    Player* player = nullptr;

    cout << "\tPlease enter name and city of the team: ";
    cout << "\n\tName: "; getline(cin,name);
    cout << "\tCity: "; getline(cin,city);
    cout << "\tEnter name of team coach: ";     getline(cin,coach);
    numPlayers = readInt("\tHow many players will be registered on the team?",1,30);

    for(int i = 1; i <= numPlayers; i++){
        player = new Player(i);      // Initialises a new player, with all values 0

        vPlayers.push_back(player);
    }
}

/**
 *  Prints team to screen, prints name, players, coach
 */
void Team::printTeam(){
    cout << "\n       " << name << endl;
    cout << "       Trainer name: " << coach << endl;
    cout << "       Team city: " << city << endl;

    cout << setw(20) << "Player Names:" << setw(5) << "   Nr:" << endl;
    for(const auto& val : vPlayers){
        val->printPlayer();
    }
}

/**
 *  Prints team to screen, prints name, players, coach
 */
void Team::printTeamGoalsPlayers(){
    cout << "\n       " << name << endl;
    cout << "       Trainer name: " << coach << endl;
    cout << "       Team city: " << city << endl;

    cout << setw(20) << "Player Names:" << setw(5) << "   Nr:" << setw(10) << "Goals:" << endl;
    for(const auto& val : vPlayers){
        val->printPlayerGoals();
    }
}

/**
 *  Prints team to screen, prints name, players, coach
 */
void Team::printMainInfo(){
    cout    << setw(20) << name
            << setw(20) << city
            << setw(15) << numPlayers
            << endl;
}

/**
 *  Prints team goals, deltagoals and minusgoals to screen for the team
 */
void Team::printTeamGoalsMain(){
    cout    << setw(20) << name
            << setw(15) << goalsScored
            << setw(15) << deltaGoals
            << setw(15) << minusGoals
            << endl;
}

/**
 *  Prints team results to screen
 */
void Team::printTeamResults(){
    cout    << setw(20) << name
            << setw(15) << totalPoints
            << setw(15) << wins
            << setw(15) << losses
            << setw(15) << draws
            << endl;
}

/**
 *  Prints team to screen, prints name, players, coach
 */
void Team::printTeamNr(){
    cout << setw(5) << teamNr;
}

/**
 *  Adds win, loss or draw variables to the team
 *
 *  @param  w - int value wins
 *  @param  l - int value losses
 *  @param  d - int value draws
 */
void Team::addWLD(int w, int l, int d){
    wins+=w;
    losses+=l;
    draws+=d;
}

/**
 *  Return team nr
 *
 *  @return teamNr - int value of team number
 */
int Team::returnTeamNr(){
    return teamNr;
}

/**
 *  Returns team name
 *
 *  @return name - string value of the team name
 */
string Team::returnNameOnly(){
    return name;
}

/**
 *  Adds goals to a specified player
 *
 *  @see    Player::printPlayer()
 *  @see    Player::addGoals()
 *  @see    Player::returnPlayer(...)
 */
void Team::addGoals(int gScored){
    int plNr;

    cout << setw(20) << "Player Names:" << setw(5) << "Nr:" << endl;
    for(const auto& val : vPlayers){
        val->printPlayer();
    }

    plNr = readInt("\tGoals scored by? (Enter player nr)",1,numPlayers);

    if(returnPlayer(plNr)!= nullptr){
        returnPlayer(plNr)->addGoals(gScored);
        goalsScored += gScored;
    }else
        cout << "\tCouldn't find a player with number " << plNr << endl;

}

/**
 *  Function that lets user add penalties to a specified player
 *
 *  @see    Player::addYellow()
 *  @see    Player::addRed()
 *  @see    Player::printPlayer()
 *  @see    Player::returnPlayer(plNr)
 */
void Team::addPenalties(){
    int plNr;
    char pType = ' ';

    cout << setw(20) << "Player Names:" << setw(5) << "Nr:" << endl;
    for(const auto& val : vPlayers){
        val->printPlayer();
    }

    cout << "\tNB: A third yellow card will automatically be registered as a red card\n" << endl;
    pType = readChar("\tEnter type of penalty, or 'Q' to quit (Y = Yellow Card, R = Red Card)");



    while(pType!='Q'){
        switch(pType){
            case 'Y':
                plNr = readInt("\tAdd penalty to which player? (Enter player nr)",1,vPlayers.size());
                vPlayers[plNr-1]->addYellow();
                penalties += 1;

                break;

            case 'R':
                plNr = readInt("\tAdd penalty to which player? (Enter player nr)",1,numPlayers);
                if(returnPlayer(plNr)!= nullptr){
                    returnPlayer(plNr)->addRed();
                    penalties += 1;
                }else{
                    cout << "\tCouldn't find a player with number " << plNr << endl;
                }
                break;

            default:
                cout << "\tInvalid character entered" << endl;
                break;
        }
        pType = readChar("\tEnter type of penalty, or 'Q' to quit (Y = Yellow Card, R = Red Card)");
    }
}

/**
 *  Function that prints players with penalties (and the types) to screen. But only the ones that have penalties
 *
 *  @see    Player::hasPenalties()
 *  @see    Player::printPlayerPenalties()
 */
void Team::viewPenalties(){
    if(hasPenalties()){
        for(const auto & val : vPlayers){
            if(val->hasPenalties()){
                cout << setw(20) << name;
                val->printPlayerPenalties();
            }
        }
    }
}

/**
 *  Function that adds minusgoals (param) when the opposing team scores
 *  in a match
 *
 *  @param  mG - minusGoals variable of the Team
 */
void Team::addMinusGoals(int mG){
    minusGoals += mG;
}

/**
 *  Function that computes delta goals of a team in a match
 *
 */
void Team::addDeltaGoals(){
    deltaGoals = goalsScored - minusGoals;
}

/**
 *  TODO: function that prints stats for one specific team to screen
 */
void Team::viewStats(){

}

/**
 *  Helper function, adds a specific number of points to the total match score,
 *  based on win, loss, or draw (i.e. 2, 1, or 0 pts)
 *
 *  @param  matchPts    -   number of points added to the totalPoints variable
 */
void Team::addMatchPoints(int matchPts){
    totalPoints += matchPts;
}

/**
 *  Returns the totalPoints variable value, it will vary depending on matches played and won
 *
 *  @return totalPoints - variable returned, total points gathered from wins or draws
 */
int Team::getTotalPoints(){
    return totalPoints;
}

/**
 *  Returns the goalsScored variable value, it will vary depending on matches played and won
 *
 *  @return goalsScored - variable returned, total goals scored during the tournament
 */
int Team::returnGoalsScored(){
    return goalsScored;
}

/**
 *  Returns the deltaGoals variable value, it will vary depending on matches played and won
 *
 *  @return deltaGoals - variable returned, delta goals scored in the matches, this accumulates
 */
int Team::returnDeltaGoals(){
    return deltaGoals;
}

/**
 *  Returns the deltaGoals variable value, it will vary depending on matches played and won
 *
 *  @return deltaGoals - variable returned, delta goals scored in the matches, this accumulates
 */
bool Team::hasPenalties(){
    if(penalties>0){
        return true;
    }else
        return false;
}

/**
 *  Returns a pointer to the player based on the parameter nr = number, or a nullpointer
 *
 *  @param      nr        -   the number of the player that is to be returned
 *
 *  @return     Player*   -   a pointer to the player with number nr, or nullptr
 */
Player* Team::returnPlayer(int nr){
    Player* returnPl = nullptr;

    for(const auto& val : vPlayers){
        if(val->returnPlayerNr() == nr)
            returnPl = val;
    }
    return returnPl;        // if matching, returns player w/ number = nr, else nullptr
}

/**
 *  Returns the players of the team as a vector
 *
 *  @return     vPlayers   -   the entire vector of players
 */
vector <Player*> Team::retPlayers(){
    return vPlayers;
}

/**
 *  Writes data to file
 *
 *  @param   out  - The target file for writing
 */
void Team::toFile(ofstream & out) const {
    out << name << '\n';
    out << city << '\n';
    out << coach << '\n';

    out << teamNr << ' ' << totalPoints << ' ' << penalties << '\n';
    out << goalsScored << ' ' << deltaGoals << ' ' << minusGoals << '\n';

    out << numPlayers << '\n';

    for(int i = 0; i < numPlayers; i++){
        vPlayers[i]->toFile(out);
    }
}

//-------------------------------------------------
// Match()
//.................................................
/**
 *  Constructor without parameters
 *
 */
Match::Match(){
    vSetup = {};
    goals1 = goals2 = 0;
    type = unDefined;
    completed = created = false;
}

/**
 *  Constructor with parameter
 *
 *  @param  n - match number in the vector it's registered in
 */
Match::Match(int n){
    matchNr = n;

    vSetup = {};
    goals1 = goals2 = 0;
    type = unDefined;
    completed = created = false;
}


/**
 *  Constructor, reads match from file
 *
 *  @param  in - file that is read from
 */
Match::Match(ifstream & in){
    Team* team1 = nullptr;
    Team* team2 = nullptr;
    int comp = 0;
    goals1 = goals2 = 0;

    vSetup = {};

    string t1, t2;

    in >> matchNr; in.ignore();
    getline(in,t1);
    getline(in,t2);

    if(findTeam(t1)!= nullptr)
        team1 = findTeam(t1);
    if(findTeam(t2)!= nullptr)
        team2 = findTeam(t2);

    vSetup.push_back(team1);
    vSetup.push_back(team2);

    created = true;

    in >> goals1 >> goals2; in.ignore();
    in >> comp; in.ignore();

    if(comp==1){
        completed = true;
    }else{
        completed = false;
    }
}

/**
 *  Destructor, deletes all teams in the match (from vSetup)
 *
 */
Match::~Match(){
    while (!vSetup.empty()) {                     //  Still elements in vSetup
        delete vSetup.back();                     //  Deleting last element, removing last pointer reference
        vSetup.pop_back();
    }
}

/**
 *  Enters the two opposing teams to vSetup vector in Match
 *
 */
void Match::enterTeams(){
    Team* team1 = nullptr;
    Team* team2 = nullptr;
    int t1, t2;

    cout << "\tAvailable teams, choose two from the team list: " << endl;
    for(const auto& val : gTeams){
        val->printTeamNr(); val->printMainInfo();
    }

    t1 = readInt("\tFirst team, number : ",1,gTeams.size());
    t2 = readInt("\tSecond team, number : ",1,gTeams.size());

    while(t2==t1){
        cout << "\tTeam one can't be same as team 2" << endl;
        t2 = readInt("\tSecond team, number : ",1,gTeams.size());
    }

    if(findTeam(t1)!= nullptr)
        team1 = findTeam(t1);
    if(findTeam(t2)!= nullptr)
        team2 = findTeam(t2);

    vSetup.push_back(team1);
    vSetup.push_back(team2);

    created = true;
}

/**
 *  Enters two teams from parameter
 *
 *  @param  t1 - team 1 pointer, added into the vector vSetup
 *  @param  t2 - team 2 pointer, added into the vector vSetup
 */
void Match::enterTeams(Team* t1,Team* t2){
    vSetup = {};

    Team* team1 = nullptr;
    Team* team2 = nullptr;

    if(t1 != nullptr){
        team1 = t1;
    }
    if(t2 != nullptr && t1 != t2){
        team2 = t2;
    }

    vSetup.push_back(team1);
    vSetup.push_back(team2);
}

/**
 *  Helper function, sets the match type enum value to qualifier
 *
 */
void Match::setAsQuali(){
    type = qualifier;
}

/**
 *  Helper function, sets the match type enum value to quarterFinal
 *
 */
void Match::setAsQuarter(){
    type = quarterFinal;
}

/**
 *  Helper function, sets the match type enum value to semiFinal
 *
 */
void Match::setAsSemi(){
    type = semiFinal;
}

/**
 *  Helper function, sets the match type enum value to finale
 *
 */
void Match::setAsFin(){
    type = finale;
}

/**
 *  Adds goals to the desired team of the match, and minusgoals to the other
 *
 */
void Match::addMatchStats(){

    int tempNr = 0;
    int tempGoals = 0;

    cout << "\tChoose which team (1 or 2) to add goals to, or press 0 to exit\n";

    for(int i = 0; i<vSetup.size(); i++){
        cout << "\nTeam nr: " << i+1 << endl; vSetup[i]->printMainInfo();
    }

    tempNr = readInt("\tTeam nr: ",1,2);
    tempGoals = readInt("\tNumber of goals to be added: ",0,200);

    if(tempNr!=0){
        switch(tempNr){
        case 1:
            vSetup[0]->addGoals(tempGoals);
            vSetup[1]->addMinusGoals(tempGoals);
            goals1 += tempGoals;
        break;

        case 2:
            vSetup[1]->addGoals(tempGoals);
            vSetup[0]->addMinusGoals(tempGoals);
            goals2 += tempGoals;
            break;

        default:
            cout << "\tInvalid number entered, choose team 1 or 2, or press 0 to exit\n";
            break;

        }
        /*
        tempNr = readInt("\tTeam nr: ",1,2);
        tempGoals = readInt("\tNumber of goals to be added: ",0,200);
        */
    }
}

/**
 *  Prints the names and numbers of the registered teams in the one match
 *
 *  @see    Team::returnNameOnly()
 *  @see    Team::returnTeamNr()
 *
 */
void Match::printMainInfo(){
    cout << "Match " << matchNr << ":\n";
    cout << "\t\tTeam " << vSetup[0]->returnTeamNr()  << " : " << vSetup[0]->returnNameOnly()
        << "\n\t\t vs. \n" << "\t\tTeam " << vSetup[1]->returnTeamNr()  << " : " << vSetup[1]->returnNameOnly() << "\n";
}

/**
 *  Sets results based on which team has scored the most goals
 *
 *  @see    Team::returnNameOnly()
 *  @see    Team::returnTeamNr()
 *
 */
void Match::setResult(){
        int delta1,delta2;
        completed = true;

        if(goals1>goals2){  // Team 1 wins
        outcome = teamOne;
        vSetup[0]->addMatchPoints(2);
        vSetup[0]->addWLD(1,0,0);
        vSetup[1]->addWLD(0,1,0);
    }else
        if(goals2>goals1){  // Team 2 wins
            outcome = teamTwo;
            vSetup[1]->addMatchPoints(2);
            vSetup[1]->addWLD(1,0,0);
            vSetup[0]->addWLD(0,1,0);
    }else
        if(goals1==goals2){ // Draw
            outcome = draw;
            vSetup[0]->addMatchPoints(1);
            vSetup[1]->addMatchPoints(1);
            vSetup[0]->addWLD(0,0,1);
            vSetup[1]->addWLD(0,0,1);
    }
    vSetup[0]->addDeltaGoals();
    vSetup[1]->addDeltaGoals();

 }


/**
 *  Views the results of the match, i.e. winner team and loser team, or if it's a draw.
 *  Same as view match outcome: round (nr) from the wireframe
 *
 *  @see    Team::returnNameOnly()
 *  @see    Team::returnTeamNr()
 *
 */
void Match::viewMatchResult(){
    cout << "\n\tMatch nr. " << matchNr << endl;
    if(completed){
        switch(outcome){
        case teamOne:
            cout << "\tWin: Team nr." << vSetup[0]->returnTeamNr() << " " << vSetup[0]->returnNameOnly() << endl;
            cout << "\tLoss: Team nr." << vSetup[1]->returnTeamNr() << " " << vSetup[1]->returnNameOnly()<< endl;
            break;
        case teamTwo:
            cout << "\tWin: Team nr." << vSetup[1]->returnTeamNr() << " " << vSetup[1]->returnNameOnly() << endl;
            cout << "\tLoss: Team nr." << vSetup[0]->returnTeamNr() << " " << vSetup[0]->returnNameOnly()<< endl;
            break;
        case draw:
            cout << "\tDraw: Team nr." << vSetup[0]->returnTeamNr() << " " << vSetup[0]->returnNameOnly()
                <<  "\tand Team nr." << vSetup[1]->returnTeamNr() << " " << vSetup[1]->returnNameOnly()<< endl;
            break;
        default:
            cout << "\tResult not set yet." << endl;
            break;
        }
    }else{
        cout << "\tMatches in this round have not been finalized/completed yet. Please do this first to view results" << endl;
    }
}

/**
 *  Returns the winner of the match, or nullptr if it's a complete draw
 *
 */
Team* Match::returnWinner(){
    Team* winningTeam = nullptr;

    if(goals1>goals2){
        winningTeam = vSetup[0];
    }
    if(goals1<goals2){
        winningTeam = vSetup[1];
    }

    return winningTeam;
}

/**
 *  Returns the loser of the match
 *
 */
Team* Match::returnLoser(){
    Team* losingTeam = nullptr;

    if(vSetup[0]->getTotalPoints()<vSetup[1]->getTotalPoints()){
        losingTeam = vSetup[0];
    }
    else{
        losingTeam = vSetup[1];
    }

    return losingTeam;
}

/**
 *  Returns the team with the highest score of the match, called if it's a draw
 *  or nullptr if it's a complete draw
 *
 */
Team* Match::compareGoals(){
    Team* tempTeam = nullptr;

    if(vSetup[0]->returnGoalsScored()<vSetup[1]->returnGoalsScored()){
        tempTeam = vSetup[1];
    }
    else
    if(vSetup[0]->returnGoalsScored()>vSetup[1]->returnGoalsScored()){
        tempTeam = vSetup[0];
    }else
    if(vSetup[0]->returnGoalsScored()>vSetup[1]->returnGoalsScored()){
        if(vSetup[0]->returnDeltaGoals()<vSetup[1]->returnDeltaGoals()){
            tempTeam = vSetup[1];
        }else
        if(vSetup[0]->returnDeltaGoals()>vSetup[1]->returnDeltaGoals()){
            tempTeam = vSetup[0];
        }
        else{
            tempTeam = nullptr;
        }
    }
    return tempTeam;
}

/**
 *  Returns true if the match is created and has two teams in vSetup
 *
 */
bool Match::isCreated(){
    return created;
}

/**
 *  Returns true if the match is created and has two teams in vSetup
 *
 */
bool Match::isComplete(){
    if(outcome!=notSet&&completed){
        return true;
    }else{
        return false;
    }
}

/**
 *  Returns true if the match is created and has two teams in vSetup
 *
 */
int Match::returnMatchNr(){
    return matchNr;
}

/**
 *  Writes match to file
 *
 *  @param  out - file destination for writing
 */
void Match::toFile(ofstream & out) const{

    /*
    switch(type){
        case qualifier:     out << 'P' << '\n';     break;
        case quarterFinal:  out << 'Q' << '\n';     break;
        case semiFinal:     out << 'S' << '\n';     break;
        case finale:        out << 'F' << '\n';     break;
        default: break;
    }
    */

    out << matchNr << '\n';
    out << vSetup[0]->returnNameOnly() << '\n';
    out << vSetup[1]->returnNameOnly() << '\n';

    out << goals1 << ' ' << goals2 << '\n';
    if(completed){
        out << 1 << '\n';
    }else{
        out << 0 << '\n';
    }
}


//-------------------------------------------------
//          OTHER FUNCTION DEFINITIONS
//-------------------------------------------------

/**
 *  Reads all team information from a teams.dta text file
 *
 */
void readTeamsFromFile(){
    ifstream teamsFile("teams.dta");        // opening team-file

    if(teamsFile){
        cout << "Reading teams from file...\n\n";
        teamsFile >> numTeams; teamsFile.ignore();
        if(numTeams>0){
            for(int i = 0; i<numTeams; i++){
                gTeams.push_back(new Team(teamsFile));
            }
        }
        teamsFile.close();
    }else{
        cout << "\n\nCouldn't access the file 'teams.dta'\n\n";
    }
}

/**
 *  Reads all match information from a matches.dta file
 *
 */
void readMatchesFromFile(){
    ifstream matchFile("matches.dta");      // opening match-file
    Match* newMatch = nullptr;
    Team* tempTeam = nullptr;

    qualiWinners = {};
    quarterWinners = {};
    semiWinners = {};

    if(matchFile){
        cout << "\n\nReading matches from file...\n\n";
        matchFile >> roundParam >> roundCreated; matchFile.ignore();

        if(roundCreated>0){
        // Taken directly from match setup, rewritten a little
            switch(roundCreated){
        case 1:
            if(roundCreated==1){
                for(int i = 0; i<8; i++){
                    newMatch = new Match(matchFile);
                    newMatch->setAsQuali();
                    gQualifiers.push_back(newMatch);
                }
            }
            break;

        case 2:
            //first kvalik
            for(int i = 0; i<8; i++){
                newMatch = new Match(matchFile);
                newMatch->setAsQuali();
                gQualifiers.push_back(newMatch);
            }
            if(roundCreated==2){
                for(int i = 0; i < 8; i++){
                    tempTeam = gQualifiers[i]->returnWinner();
                    qualiWinners.push_back(tempTeam);
                }
                // then quarterfinals
                for(int i = 0; i<4; i++){
                    newMatch = new Match(matchFile);
                    newMatch->setAsQuarter();
                    gQuarters.push_back(newMatch);
                }
            }
            break;

        case 3:
            //first kvalik
            for(int i = 0; i<8; i++){
                newMatch = new Match(matchFile);
                newMatch->setAsQuali();
                gQualifiers.push_back(newMatch);
            }
            for(int i = 0; i < 8; i++){
                tempTeam = gQualifiers[i]->returnWinner();
                qualiWinners.push_back(tempTeam);
            }
            // then quarterfinals
            for(int i = 0; i<4; i++){
                newMatch = new Match(matchFile);
                newMatch->setAsQuarter();
                gQuarters.push_back(newMatch);
            }
            if(roundCreated==3){
                for(int i = 0; i < 4; i++){
                    tempTeam = gQuarters[i]->returnWinner();
                    quarterWinners.push_back(tempTeam);
                }
                // Then semifinals
                for(int i = 0; i<2; i++){
                    newMatch = new Match(matchFile);
                    newMatch->setAsSemi();
                    gSemifinals.push_back(newMatch);
                }
            }
            break;

        case 4:
            //first kvalik
            for(int i = 0; i<8; i++){
                newMatch = new Match(matchFile);
                newMatch->setAsQuali();
                gQualifiers.push_back(newMatch);
            }
            for(int i = 0; i < 8; i++){
                tempTeam = gQualifiers[i]->returnWinner();
                qualiWinners.push_back(tempTeam);
            }
            // then quarterfinals
            for(int i = 0; i<4; i++){
                newMatch = new Match(matchFile);
                newMatch->setAsQuarter();
                gQuarters.push_back(newMatch);
            }
            for(int i = 0; i < 4; i++){
                tempTeam = gQuarters[i]->returnWinner();
                quarterWinners.push_back(tempTeam);
            }
            // Then semifinals
            for(int i = 0; i<2; i++){
                newMatch = new Match(matchFile);
                newMatch->setAsSemi();
                gSemifinals.push_back(newMatch);
            }
            if(roundCreated==4){
               for(int i = 0; i < 2; i++){
                    tempTeam = gSemifinals[i]->returnWinner();
                    semiWinners.push_back(tempTeam);
                }
                // then finale
                newMatch = new Match(matchFile);
                newMatch->setAsFin();
                gFinale = newMatch;
            }
            break;

        default:
            break;
        }
    }else{
            cout << "\n\nNo matches have been created yet for any round\n\n";
        }
    }else{
        cout << "\n\nCouldn't find the file 'matches.dta'..\n\n";
    }
}

/**
 *  Displays main menu with all available commands
 */
void displayMenu(){
    cout    << "\n\t---Main Menu---" << endl;
    cout    << "\n Available Commands: \n"
            << "\t'T' - Team Admin\n"
            << "\t'M' - Match Admin\n"
            << "\t'G' - Goals\n"
            << "\t'R' - Results\n"
            << "\t'S' - Stats\n\n"

            << "\t'Q' - Quit Program\n\n";
}

/**
 *  Displays the Team Admin menu, available commands
 */
void teamMenu(){
    cout    << "\n\t---Team Admin---" << endl;
    cout    << "\n Available Commands: \n"
            << "\t'N' - Register New Team\n"
            << "\t'A' - View All Teams\n"
            << "\t'T' - View One Specific Team\n"
            << "\t'P' - Add Penalty to a Player\n\n"

            << "\t'Q' - Quit to Main Menu\n\n";
}

/**
 *  Displays the Match Admin menu, available commands
 */
void matchMenu(){
    cout    << "\n\t---Match Admin---" << endl;
    cout    << "\n Available Commands: \n"
            << "\t'S' - Setup Matches \n"
            << "\t'V' - View Current Match Setup\n\n"

            << "\t'Q' - Quit to Main Menu\n\n";
}

/**
 *  Displays the Goals menu, available commands
 */
void goalsMenu(){
    cout    << "\n\t---Goals---" << endl;
    cout    << "\n Available Commands: \n"
            << "\t'A' - Add Goals to a Specific Team\n"
            << "\t'T' - View Team Goals\n"
            << "\t'P' - View Player Goals in a Specific Team\n\n"

            << "\t'Q' - Quit to Main Menu\n\n";
}

/**
 *  Displays the Results menu, available commands
 */
void resultsMenu(){
    cout    << "\n\t---Results---" << endl;
    cout    << "\n Available Commands: \n"
            << "\t'F' - Finalize Matches (automatically sets results)\n"
            << "\t'V' - View Results so far in the Tournament\n"
            << "\t'M' - View Match Results for User Specified Round\n\n"

            << "\t'Q' - Quit to Main Menu\n\n";
}

/**
 *  Displays the Stats menu, available commands
 */
void statsMenu(){
    cout    << "\n\t---Stats---" << endl;

    cout    << "\n Available Commands: \n"
            << "\t'V' - View Penalties\n"
            << "\t'P' - View Player Rankings\n"
            << "\t'T' - View Team Rankings\n\n"

            << "\t'Q' - Quit\n\n";

}

/**
 *  Adds all teams to the tournament. 16 teams total, for functions to work. Then
 *  iterates through and displays all teams in the tournament
 *
 *  @see    Team::registerTeam(...)
 *  @see    void viewAllTeams()
 */
void addTeam(){
    int nrr;
    Team* tempTeam = nullptr;
    cout << "\tRegister new team (max 16 total)" << endl;

    if(gTeams.size()<16){
        if(!gTeams.empty()){
            nrr = gTeams.back()->returnTeamNr();
            cout << "\tTeam nr " << nrr+1 << " out of " << MAX_TEAMS << " possible" << endl;
            tempTeam = new Team(nrr+1);
            gTeams.push_back(tempTeam);
        }else{
            cout << "\tTeam nr 1 out of " << MAX_TEAMS << " possible" << endl;
            tempTeam = new Team(1);
            gTeams.push_back(tempTeam);
        }
    }


    cout << "\n\tTeam registration complete\n" << endl;
    viewAllTeams();
}

/**
 *  Displays ALL info of ONE particular team, if it's found. If not, nullptr is returned
 *
 *  @see    Team* findTeam(...)
 *  @see    Team::printTeam()
 */
void viewTeam(){
    Team* newTeam = nullptr;
    int choice = 0;
    if(!gTeams.empty()){
        viewAllTeams();
        choice = readInt("\tWhat team should be viewed", 1, MAX_TEAMS);

        if(findTeam(choice)!= nullptr){
            newTeam = findTeam(choice);
            newTeam->printTeam();
        }else{
            cout << "\tCouldn't find team with ref.nr. " << choice << endl;
        }
    }else{
        cout << "\tNo teams registered yet!" << endl;
    }
}

/**
 *  Displays ALL goals of ONE particular team, if it's found. If not, nullptr is returned
 *
 *  @see    Team* findTeam(...)
 *  @see    Team::printTeamGoalsPlayers()
 */
void viewPlayerGoals(){
    Team* newTeam = nullptr;
    int choice = 0;
    if(!gTeams.empty()){
        viewAllTeams();
        choice = readInt("\tWhat team should be viewed", 1, MAX_TEAMS);

        if(findTeam(choice)!= nullptr){
            newTeam = findTeam(choice);
            newTeam->printTeamGoalsPlayers();
        }else{
            cout << "\tCouldn't find team with ref.nr. " << choice << endl;
        }
    }else{
        cout << "\tNo teams registered yet!" << endl;
    }
}

/**
 *  Displays MAIN INFO of all teams registered (ref.nr (placement in vector), name, city, numPlayers)
 *
 *  @see    Team::printTeamNr()
 *  @see    Team::printMainInfo()
 */
void viewAllTeams(){
    if(!gTeams.empty()){
        cout << "Currently Registered Teams\n" << endl;
        cout    << setw(5) << "NR."
                << setw(20) << "NAME"
                << setw(20) << "CITY"
                << setw(15) << "PLAYERS"
                << endl;
        for(const auto& val : gTeams){
            val->printTeamNr(); val->printMainInfo();
        }
    }else{
        cout << "\tNo teams have been registered yet!" << endl;
    }
}

/**
 *  Displays MAIN INFO of all teams registered, with goals scored, deltagoals and minusgoals
 *
 *  @see    Team::printTeamNr()
 *  @see    Team::printTeamGoalsMain()
 */
void viewAllTeamGoals(){
    if(!gTeams.empty()){
        cout << "Total Goals Scored\n" << endl;
        cout    << setw(5) << "NR."
                << setw(20) << "NAME"
                << setw(15) << "GOALS"
                << setw(15) << "DELTA"
                << setw(15) << "MINUS"
                << endl;
        for(const auto& val : gTeams){
            val->printTeamNr(); val->printTeamGoalsMain();
        }
    }else{
        cout << "\tNo teams have been registered yet!" << endl;
    }
}

/**
 *  Adds a penalty to user specified player
 *
 *  @see    Team::addPenalties()
 *  @see    Team::returnTeamNr()
 */
void addPenalty(){
    int teamNr = 0;
    teamNr = readInt("\tAdd penalty to player on which team?", 1,gTeams.size());

    Team* temp = nullptr;

    if(findTeam(teamNr)!=nullptr){
        temp = findTeam(teamNr);
    }

    temp->addPenalties();
}

/**
 *  Views penalties of players across all teams, including types (red & yellow cards)
 *
 *  @see    void Team::viewPenalties()
 *  @see    bool anyPenalties()
 */
void viewPenalties(){
    if(anyPenalties()){
        cout << setw(20) << "Team:" << setw(28) << "Player:" << setw(20) << "Yellow Cards:" << setw(20) << "Red Cards:" << endl;
        for(const auto & val : gTeams){
            val->viewPenalties();
        }
    }else{
        cout << "\tNo penalties have been registered yet.\n";
    }
}

/**
 *  Increases roundparam if all matches in the round is set to completed
 */
void increaseRoundParam(vector <Match*> m){
    int inc = 0;
    int n = 0;
    vector <int> j = {};

    for(int i =0; i < m.size(); i++){
        if(m[i]->isComplete()){
            inc+=1;
        }else{
            n+=1;
            j.push_back(i);
        }
    }

    if(inc == m.size()){
        roundParam+=1;
    }
    else{
        cout << "\tNot all matches in this round are finalized yet, please finalize the following matches before exiting: " << endl;
        for(int i = 0; i<n; i++){
            m[j.at(i)]->printMainInfo();
        }
    }
}

/**
 *  Overloaded function, increases roundparam if the match is set to completed
 */
void increaseRoundParam(Match* m){

    if(m->isComplete()){
        roundParam+=1;
    }
    else{
        cout << "\tThe match is not finalized yet, please do this first " << endl;
    }
}

/**
 *  Sets results of a match (who won, who lost, or if it's a draw). Then adds totalpoints to the winning team,
 *  increments all appropriate values
 *
 *  @see    bool Match::isComplete()
 *  @see    void Match::setResult()
 *  @see    void Match::printMainInfo()
 *  @see    void increaseRoundParam(...)
 */
void finalize(){ // Same as finalize()
    cout << "\tFinalize what types of matches? (press 'E' to exit)" << endl;

    char typeOfMatch = ' ';

    cout << "\tAvailable commands: \n\t\t'P' = Preliminary/Qualifyer \n\t\t'Q' = Quarter-final  \n\t\t'S' = Semi-final \n\t\t'F' = Finale)" << endl;
    typeOfMatch = readChar("Command");

    while(typeOfMatch!='E'){
        switch(typeOfMatch){
            case 'P':
                if(!gQualifiers.empty() && gQualifiers.size()==8 && roundParam==1){
                    cout << "\tFinalizing all qualification round matches..." << endl;
                    for(const auto& val : gQualifiers){
                        val->printMainInfo();
                        if(!val->isComplete()){
                            val->setResult();
                            cout << "\tMatch nr." << val->returnMatchNr() <<" finalized, results are calculated and stored..." << endl;
                        }else{
                            cout << "\tMatch has already been finalized" << endl;
                        }
                    }
                    increaseRoundParam(gQualifiers);
                }else{
                    cout << "\tThe Qualifying matches have not yet been set up. Please initialize the round first. To do this, the Qualifying matches first need to be completed and finalized." << endl;
                }
                break;

            case 'Q':
                if(!gQuarters.empty() && gQuarters.size()==4 && roundParam==2){
                    cout << "\tFinalizing all Quarter-final matches..." << endl;
                    for(const auto& val : gQuarters){
                        val->printMainInfo();
                        if(!val->isComplete()){
                            val->setResult();
                            cout << "\tMatch nr." << val->returnMatchNr() <<" finalized, results are calculated and stored..." << endl;
                        }else{
                            cout << "\tMatch has already been finalized" << endl;
                        }
                    }
                    increaseRoundParam(gQuarters);
                }else{
                    cout << "\tThe Quarter-finals have not yet been set up. Please initialize the round first. To do this, the Qualifying matches first need to be completed and finalized." << endl;
                }
                break;

            case 'S':
                if(!gSemifinals.empty() && gSemifinals.size()==2 && roundParam==3){
                    cout << "\tFinalizing all Semi-final matches..." << endl;
                    for(const auto& val : gSemifinals){
                        val->printMainInfo();
                        if(!val->isComplete()){
                            val->setResult();
                            cout << "\tMatch nr." << val->returnMatchNr() <<" finalized, results are calculated and stored..." << endl;
                        }else{
                            cout << "\tMatch has already been finalized" << endl;
                        }
                    }
                    increaseRoundParam(gSemifinals);
                }else{
                    cout << "\tThe Semi-finals have not yet been set up. Please initialize the round first. To do this, the Quarter-finals first need to be completed and finalized." << endl;
                }
                break;

            case 'F':
                cout << "\tAccessing finale" << endl;
                if(gFinale != nullptr && roundParam==4){
                    gFinale->printMainInfo();
                    if(!gFinale->isComplete()){
                        gFinale->setResult();
                        cout << "\tMatch finalized, results are calculated" << endl;
                        increaseRoundParam(gFinale);
                    }else{
                        cout << "\tMatch has already been finalized" << endl;
                    }
                } else{
                    cout << "\tThe finale has not yet been set up. Please initialize it first. To do this, the Semi-finals first need to be completed and finalized." << endl;
                }
                break;

           default:
               cout << "\tIncorrect character value typed. Available commands: 'P' 'Q' 'S' 'F' " << endl;
               break;
        }
        cout << "\tFinalize another match, or press 'E' to exit" << endl;
        typeOfMatch = readChar("Command");
    }
}

/**
 *  Prints all results to the screen. Should depend on a roundParam
 *
 *  @see    Team::printTeamResults()
 */
void viewResults(){
    // some sort() function based on totalpoints in the gTeams ?

    if(!gTeams.empty()){
        cout << setw(20) << "TEAM NAME" << setw(15) << "POINTS" << setw(15) << "WINS" << setw(15) << "LOSSES" << setw(15) << "DRAWS" << endl;
        for(const auto& val : gTeams){
            val->printTeamResults();
        }
    }else{
        cout << "\tNo teams registered yet" << endl;
    }

}

/**
 *  Prints a list of the 20 top scoring players
 *
 *  @see    Teams::retPlayers()
 *  @see    Player::retGoals()
 *  @see    Player::printPlayerGoals()
 */
void viewPlayerRankings(){

    if(!gTeams.empty()){

        vector <Player*> sortedPlayers = {};
        for(const auto & val : gTeams){
            for(const auto & i : val->retPlayers()){
                sortedPlayers.push_back(i);
            }
        }

        std::sort(sortedPlayers.begin(), sortedPlayers.end(), [&](Player* t1, Player* t2) {
            return t1->retGoals() > t2->retGoals();
        });

            cout << "\nPlayer Rankings\n\n";
            cout << setw(18) << "Name:" << setw(10) << "Number:" << setw(10) << "Goals:\n";
        for(int i = 0; i<20; i++){
            sortedPlayers[i]->printPlayerGoals();
        }
    }else{
        cout << "\tNo teams registered yet.\n";
    }
}

/**
 *  Team Rankings - sorts on totalpoints, displays goalsscored as well
 *
 *  @see    Team::getTotalPoints()
 *  @see    Team::returnNameOnly()
 */
void viewTeamRankings(){
    vector <Team*> sortedTeams = {};

    if(!gTeams.empty()){

        for(const auto & val : gTeams){
            sortedTeams.push_back(val);
        }

        std::sort(sortedTeams.begin(), sortedTeams.end(), [&](Team* t1, Team* t2) {
                return t1->getTotalPoints() > t2->getTotalPoints();
            });

            cout << "\nTeam Rankings\n\n";
            cout << setw(15) << "Ranking:" << setw(20) << "Team:" << setw(15) << "Points:\n";
        for(int i = 0; i < sortedTeams.size(); i++){
            cout << setw(15) << i+1 << setw(20) << sortedTeams[i]->returnNameOnly() << setw(15) << sortedTeams[i]->getTotalPoints() << endl;
        }
    }else{
        cout << "\tNo teams registered yet.\n";
    }

}

/**
 *  Prints all results of one specified round to the screen
 */
void viewMatchResults(){
    char typeOfMatch = ' ';

    cout << "\tChoose match type (aka. round) you want to view outcomes for, or press 'E' to exit" << endl;
    cout << "\tAvailable commands: \n\t\t'P' = qualifyer \n\t\t'Q' = quarter-final  \n\t\t'S' = semi-final \n\t\t'F' = finale)" << endl;
    typeOfMatch = readChar("Command");

    while(typeOfMatch!='E'){
        switch(typeOfMatch){
            case 'P':
                if(!gQualifiers.empty()){
                    for(const auto& val : gQualifiers){
                        if(val->isComplete()){
                            val->viewMatchResult();
                        }else{
                            cout << "\tMatch nr. " << val->returnMatchNr() << " in preliminary round is not finalized/completed yet. Please do this first\n" << endl;
                        }
                    }
                }else{
                    cout << "\tQualifying/Preliminary matches have not yet been set up. Please do this first\n" << endl;
                }
                break;

            case 'Q':
                if(!gQuarters.empty()){
                    for(const auto& val : gQuarters){
                       if(val->isComplete()){
                            val->viewMatchResult();
                        }else{
                            cout << "\tMatch nr. " << val->returnMatchNr() << " in quarter-finals is not finalized/completed yet. Please do this first\n" << endl;
                        }
                    }
                }else{
                    cout << "\tQuarter-finals have not yet been set up. Please do this first\n" << endl;
                }
                break;

            case 'S':
                if(!gSemifinals.empty()){
                    for(const auto& val : gSemifinals){
                        if(val->isComplete()){
                            val->viewMatchResult();
                        }else{
                            cout << "\tMatch nr. " << val->returnMatchNr() << " in semi-finals is not finalized/completed yet. Please do this first\n" << endl;
                        }
                    }
                }else{
                    cout << "\tSemi-finals have not yet been set up. Please do this first\n" << endl;
                }
                break;

            case 'F':
                if(gFinale!=nullptr){
                    cout << "\tAccessing finale" << endl;
                    if(gFinale->isComplete()){
                            gFinale->viewMatchResult();
                        }else{
                            cout << "\tThe finale has not been finalized/completed yet. Please do this first\n" << endl;
                        }
                    }else{
                    cout << "\tFinale has not yet been set up. Please do this first\n" << endl;
                }
                break;

           default:
               cout << "\tUnavailable character value typed. Available commands: 'P' 'Q' 'S' 'F' " << endl;
               break;
        }
        cout << "\tView results from another round('P','Q','S','F'), or press 'E' to exit" << endl;
        typeOfMatch = readChar("Command");
    }
}

/**
 *  Finds the team in the gTeams, and then adds goals to the correct player + overall goals
 *  scored by the team respectively ( put this as a match function, e.g. match::addmatchstats)
 *
 *  @see    Match::printMainInfo()
 *  @see    Match::addMatchStats()
 */
void addGoalsScored(){
    char typeOfMatch = ' ';
    int matchNumber = 0;

    cout << "\tChoose match type you want to add goals to, or press 'E' to exit" << endl;
    cout << "\tAvailable commands: \n\t\t'P' = qualifyer \n\t\t'Q' = quarter-final  \n\t\t'S' = semi-final \n\t\t'F' = finale)" << endl;
    typeOfMatch = readChar("Command");

    while(typeOfMatch!='E'){
        switch(typeOfMatch){
            case 'P':
                if(!gQualifiers.empty()){
                    for(const auto& val : gQualifiers){
                        val->printMainInfo();
                    }
                    matchNumber = readInt("\tWhich match?",1,8);
                    gQualifiers[matchNumber-1]->addMatchStats();
                }else{
                    cout << "\tNo matches have been created yet, please do this first" << endl;
                    }
                break;

            case 'Q':
                if(!gQuarters.empty()){
                    for(const auto& val : gQuarters){
                        val->printMainInfo();
                    }
                    matchNumber = readInt("\tWhich match?",1,4);
                    gQuarters[matchNumber-1]->addMatchStats();
                }else{
                    cout << "\tNo matches have been created yet, please do this first" << endl;
                    }
                break;

            case 'S':
                if(!gSemifinals.empty()){
                    for(const auto& val : gSemifinals){
                        val->printMainInfo();
                    }
                    matchNumber = readInt("\tWhich match?",1,2);
                    gSemifinals[matchNumber-1]->addMatchStats();
                }else{
                    cout << "\tNo matches have been created yet, please do this first" << endl;
                    }
                break;

            case 'F':
                if(gFinale!= nullptr){
                    cout << "\tAccessing finale" << endl;
                    gFinale->addMatchStats();
                }else{
                    cout << "\tFinale has not been created yet, please do this first" << endl;
                }
                break;

           default:
               cout << "\tUnavailable character value typed. Available commands: 'P' 'Q' 'S' 'F' " << endl;
               break;
        }
        cout << "\tAdd goals to another match('P','Q','S','F'), or press 'E' to exit" << endl;
        typeOfMatch = readChar("Command");
    }
}

/**
 *  Creates a match setup based on what round it is in the tournament. Automatic (winners go thru)
 *  after the qualifying rounds, added to the correct vector
 *
 *  @see    Match::enterTeams()
 *  @see    Match::enterTeam1(...)
 *  @see    Match::enterTeam2(...)
 *  @see    Match::setAsQuali()
 *  @see    Match::setAsQuarter()
 *  @see    Match::setAsSemi()
 *  @see    Match::setAsFin()
 *  @see    Match::returnWinner()
 */
void createMatchSetup(){
    int numMatches;
    Match* newMatch = nullptr;
    Team* tempTeam = nullptr;

    qualiWinners = {};
    quarterWinners = {};
    semiWinners = {};


    switch(roundParam){
    case 1:
        numMatches = 8;
        for(int i = 0; i<numMatches; i++){
            newMatch = new Match(i+1);
            newMatch->setAsQuali();
            newMatch->enterTeams();
            gQualifiers.push_back(newMatch);   // Change to appropriate vector if needed
        }
        roundCreated=1;
        break;

    case 2:
        numMatches = 4;
        for(int i = 0; i < 8; i++){
            tempTeam = gQualifiers[i]->returnWinner();
            qualiWinners.push_back(tempTeam);
        }
        cout << "\n\nWinners of preliminary round:\n\n";
        for(const auto & val : qualiWinners){
            val->printMainInfo();
        }
        for(int i = 0; i<numMatches; i++){
            newMatch = new Match(i+1);
            newMatch->setAsQuarter();
            newMatch->enterTeams();
            gQuarters.push_back(newMatch);
        }
        roundCreated=2;
        break;

    case 3:
        numMatches = 2;
        for(int i = 0; i < 4; i++){
            tempTeam = gQuarters[i]->returnWinner();
            quarterWinners.push_back(tempTeam);
        }

        cout << "\n\nWinners of quarter-final round:\n\n";
        for(const auto & val : quarterWinners){
            val->printMainInfo();
        }
        for(int i = 0; i<numMatches; i++){
            newMatch = new Match(i+1);
            newMatch->setAsSemi();
            newMatch->enterTeams();
            gSemifinals.push_back(newMatch);
        }
        roundCreated=3;
        break;

    case 4:

       for(int i = 0; i < 2; i++){
            tempTeam = gSemifinals[i]->returnWinner();
            semiWinners.push_back(tempTeam);
        }
        cout << "\n\nWinners of semi-final round:\n\n";
        for(const auto & val : semiWinners){
            val->printMainInfo();
        }
        newMatch = new Match(1);
        newMatch->setAsFin();
        newMatch->enterTeams();
        gFinale = newMatch; // or, you can set gFinale = newMatch
        roundCreated=4;
        break;

    default:
        break;
    }
}

/**
 *  Prints match setup based on how many rounds of matches have been set up (and not completed).
 *  Tries to order in a logical manner
 *
 *  @see    Match::printMainInfo()
 *  @see    Team::returnTeamNr()
 *  @see    Team::returnNameOnly()
 */
void viewMatchSetup(){

    switch(roundCreated){
    case 1:
        // Quals
        cout << "\tPreliminary/qualifying matches:\n" << endl;
        for(const auto& val : gQualifiers){
            val->printMainInfo();
        }
        break;
    case 2:
        // Quals
        cout << "\tPreliminary/qualifying matches:\n" << endl;
        for(const auto& val : gQualifiers){
            val->printMainInfo();
        }
        // Quarts
        cout << "\tQuarter-finals:\n" << endl;
        for(const auto& val : gQuarters){
            val->printMainInfo();
        }
        break;
    case 3:
        // Quals
        cout << "\tPreliminary/qualifying matches:\n" << endl;
        for(const auto& val : gQualifiers){
            val->printMainInfo();
        }
        // Quarts
        cout << "\tQuarter-finals:\n" << endl;
        for(const auto& val : gQuarters){
            val->printMainInfo();
        }
        // Semi
        cout << "\tSemi-finals:\n" << endl;
        for(const auto& val : gSemifinals){
            val->printMainInfo();
        }
        break;

    case 4:
        // Quals
        cout << "\tPreliminary/qualifying matches:\n" << endl;
        for(const auto& val : gQualifiers){
            val->printMainInfo();
        }
        // Quarts
        cout << "\tQuarter-finals:\n" << endl;
        for(const auto& val : gQuarters){
            val->printMainInfo();
        }
        // Semi
        cout << "\tSemi-finals:\n" << endl;
        for(const auto& val : gSemifinals){
            val->printMainInfo();
        }
        // Finale
        cout << "\tFinale:\n" << endl;
        gFinale->printMainInfo();
        break;

    case 5:
        // Quals
        cout << "\tPreliminary/qualifying matches:\n" << endl;
        for(const auto& val : gQualifiers){
            val->printMainInfo();
        }
        // Quarts
        cout << "\tQuarter-finals:\n" << endl;
        for(const auto& val : gQuarters){
            val->printMainInfo();
        }
        // Semi
        cout << "\tSemi-finals:\n" << endl;
        for(const auto& val : gSemifinals){
            val->printMainInfo();
        }
        // Finale
        cout << "\tFinale:\n" << endl;
        gFinale->printMainInfo();
        // Winner
        cout << "\tWinner:\n" << endl;
        cout << "\t\tTeam " << gWinner->returnTeamNr()  << " : " << gWinner->returnNameOnly() << endl;
        break;
    default:
        break;
    }
}

/**
 *  "Team Admin" command, function called from the main menu. Can register new team, view all teams
 *  view one specific team based on its nr in gTeams vector, and add a penalty to a player on a team
 *
 *  @see    void teamMenu()
 *  @see    void addTeam()
 *  @see    void viewTeam()
 *  @see    void viewAllTeams()
 *  @see    void addPenalty()
 */
void team(){
    char command;

    teamMenu();
    command = readChar("Please enter command");

    while(command != 'Q'){
        switch(command){
            case 'N':   addTeam();          break;
            case 'A':   viewAllTeams();     break;
            case 'T':   viewTeam();         break;
            case 'P':   addPenalty();       break;
            default:    cout << "\tInvalid command\n" << endl; /*teamMenu();*/         break;
        }
        teamMenu();
        command = readChar("Please enter command");
    }
}

/**
 *  Matche Admin page, accessed from the main menu. For creating and viewing match setup
 *
 *  @see    createMatchSetup()
 *  @see    viewMatchSetup()
 *  @see    matchMenu()
 */
void matches(){
    char command;

    matchMenu();
    command = readChar("Please enter command");

    while(command != 'Q'){
        switch(command){
            case 'S':   createMatchSetup();     break;
            case 'V':   viewMatchSetup();       break;
            default:    cout << "\tInvalid command\n" << endl; /* matchMenu();  */   break;
        }
        matchMenu();
        command = readChar("Please enter command");
    }

}

/**
 *  Goals page, accessed from the main menu. addGoals and view functions
 *
 *  @param  void addGoalsScored()
 *  @param  void viewTeamGoals()
 *  @param  void viewPlayerGoals(Team* t)
 */
void goals(){
    char command;

    goalsMenu();
    command = readChar("Please enter command");

    while(command != 'Q'){
        switch(command){
            case 'A':   addGoalsScored();           break;
            case 'T':   viewAllTeamGoals();         break;
            case 'P':   viewPlayerGoals();          break;
            default:    cout << "\tInvalid command\n" << endl; /*goalsMenu(); */   break;
        }
        goalsMenu();
        command = readChar("Please enter command");
    }
}

/**
 *  The results page, accessed from the main menu
 *
 *  @see    void addResults() <-- same as finalize()
 *  @see    void viewResults()
 *  @see    void viewMatchResults(round)
 */
void results(){
    char command;

    resultsMenu();
    command = readChar("Please enter command");

    while(command != 'Q'){
        switch(command){
            case 'F':   finalize();        break;
            case 'V':   viewResults();       break;
            case 'M':   viewMatchResults();  break;
            default:    cout << "\tInvalid command\n" << endl; /* resultsMenu();  */     break;
        }
        resultsMenu();
        command = readChar("Please enter command");
    }
}

/**
 *  The stats page, accessed from the main menu. Penalties, player rankings, team rankings
 *
 *  @see    viewPenalties()
 *  @see    viewPlayerRankings()
 *  @see    viewTeamRankings()
 */
void stats(){
    char command;

    statsMenu();
    command = readChar("Please enter command");

    while(command != 'Q'){
        switch(command){

            case 'V':   viewPenalties();        break;
            case 'P':   viewPlayerRankings();   break;
            case 'T':   viewTeamRankings();     break;

            default:  cout << "\tInvalid command\n" << endl;        break;
        }
        statsMenu();
        command = readChar("Please enter command");
    }
}

/**
 *  Reads and returns one uppercase char
 *
 *  @param   t  - Text for the user
 *
 *  @return  One uppercase char.
 */
char readChar(const char* t)  {
    char c;
    std::cout << t << ":  ";
    std::cin >> c;  std::cin.ignore(MAXCHAR, '\n');
    return (toupper(c));
}


/**
 *  Reads and returns an integer between two bounds, min and max
 *
 *  @param   t    - Text to user, asking for input/a number
 *  @param   min  - Minimum for input number value
 *  @param   max  - Maksimum for input number value
 *
 *  @return  Accepted value in the interval 'min' - 'max'
 */
int readInt(const char* t, const int min, const int max)  {
    char buffer[MAXCHAR] = "";
    int  number = 0;
    bool fail = false;

    do {
        fail = false;
        std::cout << t << " (" << min << " - " << max << "):  ";
        std::cin.getline(buffer, MAXCHAR);
        number = atoi(buffer);
        if (number == 0 && buffer[0] != '0')
        {  fail = true;   std::cout << "\nERROR: Not an integer\n\n";  }
    } while (fail  ||  number < min  ||  number > max);

    return number;
}

/**
 *  Looks for a team in the gTeams vector, returns the team with name n
 *  if found, otherwise a nullptr
 *
 *  @param  n       - string value with the name of the team
 *
 *  @return Team*   - pointer to a team with name n, if found, or nullptr
 */
Team* findTeam(const string n){
    Team* tempTeam = nullptr;

    for(const auto& val : gTeams){
        if(val->returnNameOnly().compare(n)==0){ //if the string compare shows they're equal
            tempTeam = val;
        }
    }
    return tempTeam;
}

/**
 *  Looks for a team in the gTeams vector, returns the team with number nr
 *  if found, otherwise a nullptr
 *
 *  @param  nr      - int value with the number of the team
 *
 *  @return Team*   - pointer to a team with name n, if found, or nullptr
 */
Team* findTeam(const int nr){
    Team* tempTeam = nullptr;

    for(const auto& val : gTeams){
        if(val->returnTeamNr()==nr){
            tempTeam = val;
        }
    }
    return tempTeam;
}

/**
 * Checks for penalties in the global vector gTeams
 *
 *  @return true/false - depending on whether there are any penalties registered yet
 */
 bool anyPenalties(){
     int penalty = 0;
    for(const auto & val : gTeams){
        if(val->hasPenalties()){
            penalty = 1;
        }
    }

    if(penalty!=0){
        return true;
    }else
        return false;
 }

/**
 * Writes all registered teams in gTeams to a file
 *
 */
 void writeTeamsToFile(){
    ofstream outTeamsFile("teams.dta");

    cout << "\n\nWriting teams to file..\n\n";
    outTeamsFile << numTeams << '\n';

    for(const auto & val : gTeams){
        val->toFile(outTeamsFile);
    }
 }

/**
 * Writes all registered teams in gTeams to a file
 *
 *  @see    Match::toFile(...)
 */
 void writeMatchesToFile(){
    ofstream outMatchesFile("matches.dta");

    if(roundCreated>0){

        cout << "\n\nWriting matches to file...\n\n";
        outMatchesFile << roundParam << ' ' << roundCreated << '\n';

        switch(roundParam){
        case 1:
            if(roundCreated==1){
                for(int i = 0; i<8; i++){
                    gQualifiers[i]->toFile(outMatchesFile);
                }
            }
            break;

        case 2:
            // first kvalik
            for(int i = 0; i<8; i++){
                gQualifiers[i]->toFile(outMatchesFile);
            }
            if(roundCreated==2){
                // then quarterfinals
                for(int i = 0; i<4; i++){
                    gQuarters[i]->toFile(outMatchesFile);
                }
            }
            break;

        case 3:
            // first kvalik
            for(int i = 0; i<8; i++){
                gQualifiers[i]->toFile(outMatchesFile);
            }
            // then quarterfinals
            for(int i = 0; i<4; i++){
                gQuarters[i]->toFile(outMatchesFile);
            }
            if(roundCreated==3){
                // then semifinals
                for(int i = 0; i<2; i++){
                    gSemifinals[i]->toFile(outMatchesFile);
                }
            }
            break;

        case 4:
            // first kvalik
            for(int i = 0; i<8; i++){
                gQualifiers[i]->toFile(outMatchesFile);
            }
            // then quarterfinals
            for(int i = 0; i<4; i++){
                gQuarters[i]->toFile(outMatchesFile);
            }
            // then semifinals
            for(int i = 0; i<2; i++){
                gSemifinals[i]->toFile(outMatchesFile);
            }
            if(roundCreated==4){
                // then finale
                gFinale->toFile(outMatchesFile);
            }
            break;

        default:
            break;
        }
    }else{
        cout << "\n\nNo matches have been set up yet\n\n";
        outMatchesFile << roundParam << ' ' << roundCreated << '\n';
    }
 }

/*
*   Adjusting fontsize
*
*/
void fontsize(int a, int b){
  PCONSOLE_FONT_INFOEX lpConsoleCurrentFontEx = new CONSOLE_FONT_INFOEX();
  lpConsoleCurrentFontEx->cbSize = sizeof(CONSOLE_FONT_INFOEX);
  GetCurrentConsoleFontEx(out, 0, lpConsoleCurrentFontEx);
  lpConsoleCurrentFontEx->dwFontSize.X = a;
  lpConsoleCurrentFontEx->dwFontSize.Y = b;
  SetCurrentConsoleFontEx(out, 0, lpConsoleCurrentFontEx);
 }


//-------------------------------------------------

